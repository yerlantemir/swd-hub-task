import { getUsers } from 'api';
import { Button, Spinner } from 'components/atoms';
import { UserCard } from 'components/moleculas';
import { MainTemplate } from 'components/templates/MainTemplate/component';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { UserType } from 'types';
import { UsersWrapper } from './styled';

export const Users = () => {
  const [users, setUsers] = useState<UserType[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getUsers().then((data) => {
      setUsers(data);
    }).finally(() => {
      setLoading(false);
    });
  }, []);

  const removeUserLocally = (id: number) => {
    setUsers(users.filter((user) => user.userId !== id));
  };

  const history = useHistory();
  const onCreateClick = () => {
    history.push('/users/create');
  };
  return (
    <MainTemplate title="Users" noBackButton>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <div>
          {users.length === 0 && !loading
          && <p>no users</p>}
        </div>
        <Button onClick={onCreateClick}>+ Create</Button>
      </div>
      <UsersWrapper>
        {loading ? <Spinner style={{ height: 50, width: 50 }} />
          : users.length > 0 && users.map((user) => (
            <UserCard
              key={user.userId}
              removeUserLocally={
            removeUserLocally
}
              {...user}
            />
          ))}
      </UsersWrapper>
    </MainTemplate>
  );
};
