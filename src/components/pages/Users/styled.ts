import styled from 'styled-components';

export const UsersWrapper = styled.div`
    padding: 30px;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    gap: 20px;
`;
