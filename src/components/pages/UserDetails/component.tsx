import { getUser, deleteUser } from 'api';
import { Button, Spinner } from 'components/atoms';
import { ButtonVariants } from 'components/atoms/Button/types/ButtonVariantsEnum';
import { MainTemplate } from 'components/templates/MainTemplate/component';
import { deleteIcon, editIcon } from 'icons';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom';
import { UserType } from 'types';
import { DetailWrapper, StyledLabel, Wrapper } from './styled';

export const UserDetails = () => {
  const [details, setDetails] = useState<UserType>();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const { id } = useParams<{id: string}>();

  useEffect(() => {
    setLoading(true);
    getUser(parseInt(id, 10)).then((data) => {
      setDetails(data);
    }).finally(() => {
      setLoading(false);
    });
  }, [id]);

  const onDeleteClick = () => {
    setLoading(true);
    deleteUser(parseInt(id, 10)).then(() => {
      history.push('/');
    }).finally(() => {
      setLoading(false);
    });
  };

  const onEditClick = () => {
    history.push(`/users/${id}/edit`);
  };
  return (
    <MainTemplate title="User Details">
      <div style={{
        position: 'absolute', right: 40, top: 20,
      }}
      >
        {
        loading ? <Spinner style={{ height: 40, width: 40 }} /> : (

          <>
            <Button variant={ButtonVariants.Icon} style={{ marginRight: 10 }} onClick={onEditClick}>
              {editIcon}
            </Button>
            <Button variant={ButtonVariants.Icon} onClick={onDeleteClick}>
              {deleteIcon}
            </Button>
          </>
        )
      }
      </div>
      <Wrapper>
        <img
          height={150}
          width={150}
          src="https://davinci22.ru/wp-content/uploads/2014/01/default-avatar-m_1920.png"
          alt="avatar"
          style={{ borderRadius: '99999px' }}
        />

        <div style={{ marginTop: 10, paddingLeft: '50px' }}>
          <DetailWrapper>
            <StyledLabel>
              first name:
            </StyledLabel>
            <p>{details?.firstName}</p>
          </DetailWrapper>
          <DetailWrapper>
            <StyledLabel>
              last name:
            </StyledLabel>
            <p>{details?.lastName}</p>
          </DetailWrapper>
          <DetailWrapper>
            <StyledLabel>
              gender:
            </StyledLabel>
            <p>{details?.gender}</p>
          </DetailWrapper>
          <DetailWrapper>
            <StyledLabel>
              biography:
            </StyledLabel>
            <p>{details?.biography}</p>
          </DetailWrapper>
          <DetailWrapper>
            <StyledLabel>
              birthDate:
            </StyledLabel>
            <p>{details?.birthDate}</p>
          </DetailWrapper>
          <DetailWrapper>
            <StyledLabel>
              status:
            </StyledLabel>
            <p>{details?.isActive ? 'enabled' : 'disabled'}</p>
          </DetailWrapper>

        </div>
      </Wrapper>
    </MainTemplate>
  );
};
