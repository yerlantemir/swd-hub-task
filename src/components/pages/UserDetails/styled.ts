import styled from 'styled-components';

export const DetailWrapper = styled.div`
    display: flex;
    flex-direction:row;
    gap: 20px;
`;

export const StyledLabel = styled.p`
    font-weight: 600;
    font-size: 16px;
`;

export const Wrapper = styled.div`
    min-height: 500px;
    width: 40%;
    margin: auto;

    @media screen and (max-width: 1000px) {
        width: 100%;
    }
`;
