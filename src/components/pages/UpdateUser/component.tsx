import { getUser, updateUser } from 'api';
import { Spinner } from 'components/atoms';
import { UserForm } from 'components/organisms/UserForm/component';
import { MainTemplate } from 'components/templates/MainTemplate/component';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { UserType } from 'types';

export const UpdateUser = () => {
  const [details, setDetails] = useState<UserType>();
  const [loading, setLoading] = useState(false);

  const { id } = useParams<{id: string}>();

  useEffect(() => {
    setLoading(true);
    getUser(parseInt(id, 10)).then((data) => {
      setDetails(data);
    }).finally(() => {
      setLoading(false);
    });
  }, [id]);

  return (
    <MainTemplate title="Update user">

      {loading ? <Spinner style={{ height: 100, width: 100, margin: 'auto' }} /> : (
        <UserForm onSubmitClick={updateUser(parseInt(id, 10))} user={details} />
      )}
    </MainTemplate>
  );
};
