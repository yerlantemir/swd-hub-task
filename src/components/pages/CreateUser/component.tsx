import { createUser } from 'api';
import { UserForm } from 'components/organisms/UserForm/component';
import { MainTemplate } from 'components/templates/MainTemplate/component';

export const CreateUser = () => (
  <MainTemplate title="Create User">
    <UserForm onSubmitClick={createUser} />
  </MainTemplate>
);
