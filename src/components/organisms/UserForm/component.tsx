import {
  Button, Checkbox, Input, Spinner, TextArea,
} from 'components/atoms';
import { FormControl } from 'components/moleculas/FormControl';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { colors } from 'shared/colors';
import { Props } from './props';
import { UserFieldsEnum } from './types/UserFieldsEnum';

const requiredText = 'Mandatory field';

export const UserForm = ({ user, onSubmitClick, ...rest }: Props) => {
  const [enabled, setEnabled] = useState(user ? user.isActive : false);
  const { handleSubmit, register, formState: { errors } } = useForm({
    defaultValues: {
      [UserFieldsEnum.FirstName]: user?.[UserFieldsEnum.FirstName] || '',
      [UserFieldsEnum.LastName]: user?.[UserFieldsEnum.LastName] || '',
      [UserFieldsEnum.DOB]: user?.[UserFieldsEnum.DOB] || '',
      [UserFieldsEnum.Job]: user?.[UserFieldsEnum.Job] || '',
      [UserFieldsEnum.Biography]: user?.[UserFieldsEnum.Biography] || '',
      [UserFieldsEnum.Gender]: user?.[UserFieldsEnum.Gender] || '',
    },
  });
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const onSubmit = (data: any) => {
    const createData = { ...data, isActive: enabled };
    setLoading(true);
    onSubmitClick(createData).then(() => {
      history.push('/');
    }).finally(() => {
      setLoading(false);
    });
  };
  return (
    <form
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: '20px 40px',
        maxWidth: 600,
      }}
      onSubmit={handleSubmit(onSubmit)}
      {...rest}
    >

      <FormControl title="first name" errorMessage={errors[UserFieldsEnum.FirstName]?.message} style={{ width: '100%' }}>
        <Input
          {...register(UserFieldsEnum.FirstName, {
            required: requiredText,
            maxLength: {
              value: 256,
              message: 'Max length - 256',
            },
          })}
          placeholder="type here"
          style={{ width: '100%' }}
        />
      </FormControl>
      <FormControl title="last name" errorMessage={errors[UserFieldsEnum.LastName]?.message} style={{ width: '100%' }}>
        <Input
          {...register(UserFieldsEnum.LastName, {
            required: requiredText,
            maxLength: {
              value: 256,
              message: 'Max length - 256',
            },
          })}
          placeholder="type here"
          style={{ width: '100%' }}
        />
      </FormControl>
      <FormControl title="DOB" errorMessage={errors[UserFieldsEnum.DOB]?.message} style={{ width: '100%' }}>
        <Input {...register(UserFieldsEnum.DOB, { required: requiredText })} type="date" style={{ width: '100%' }} />
      </FormControl>
      <FormControl title="gender" style={{ width: '100%' }} errorMessage={errors[UserFieldsEnum.Gender]?.message}>
        <select
          {...register(UserFieldsEnum.Gender, { required: requiredText })}
          style={{
            width: '100%', height: 40, borderRadius: 6, border: '1px solid black', backgroundColor: colors.white,
          }}
        >
          <option value="male">male</option>
          <option value="female">female</option>
        </select>
      </FormControl>
      <FormControl title="job" errorMessage={errors[UserFieldsEnum.Job]?.message} style={{ width: '100%' }}>
        <Input
          {...register(UserFieldsEnum.Job, {
            required: requiredText,
            maxLength: {
              value: 256,
              message: 'Max length - 256',
            },
          })}
          placeholder="type here"
          style={{ width: '100%' }}
        />
      </FormControl>
      <FormControl title="biography" errorMessage={errors[UserFieldsEnum.Biography]?.message} style={{ width: '100%' }}>
        <TextArea
          {...register(UserFieldsEnum.Biography, {
            maxLength: {
              value: 1024,
              message: 'Max length - 1024',
            },
          })}
          placeholder="type here"
          rows={6}
          style={{ width: '100%' }}
        />
      </FormControl>
      <FormControl title="enabled">
        <Checkbox checked={enabled} setChecked={setEnabled} />
      </FormControl>
      {
          loading ? <Spinner style={{ height: 40, width: 40, margin: 'auto' }} /> : (
            <Button
              style={{
                float: 'right', marginTop: 20, width: '100%', height: 50,
              }}
              type="submit"
            >
              Submit
            </Button>

          )
        }
    </form>
  );
};
