/* eslint-disable no-unused-vars */
import { HTMLAttributes } from 'react';
import { UserType } from 'types';

export type Props = HTMLAttributes<HTMLFormElement> & {user?: UserType,
    onSubmitClick: (data: Partial<UserType>) => Promise<void>}
