/* eslint-disable no-unused-vars */
export enum UserFieldsEnum {
    FirstName = 'firstName',
    LastName = 'lastName',
    DOB = 'birthDate',
    Job = 'job',
    Biography = 'biography',
    Gender = 'gender'
}
