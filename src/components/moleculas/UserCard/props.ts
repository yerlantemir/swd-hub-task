import { UserType } from 'types';
import { CardProps } from 'components/atoms';

export type Props = CardProps & Pick<UserType, 'userId' | 'firstName' | 'lastName' | 'birthDate' | 'gender'> & {
    // eslint-disable-next-line no-unused-vars
    removeUserLocally: (id: number) => void;
};
