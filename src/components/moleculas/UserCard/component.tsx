import { Button, Card, Spinner } from 'components/atoms';
import {
  Link,
} from 'react-router-dom';
import { deleteIcon } from 'icons';
import { ButtonVariants } from 'components/atoms/Button/types/ButtonVariantsEnum';
import { useState } from 'react';
import { deleteUser } from 'api';
import { Props } from './props';

export const UserCard = ({
  userId, birthDate, firstName, lastName, gender, removeUserLocally, ...rest
}: Props) => {
  const [loading, setLoading] = useState(false);

  const onDeleteClick = () => {
    setLoading(true);
    deleteUser(userId).then(() => {
      removeUserLocally(userId);
    });
  };
  return (
    <Card {...rest} style={{ maxWidth: 250, position: 'relative' }}>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
        <Link to={`/users/${userId}`}>
          <h4 style={{ margin: 0 }}>{`${firstName} ${lastName}`}</h4>
        </Link>
        <p>{birthDate}</p>
        <p style={{ fontSize: '14px' }}>
          {gender}
        </p>
      </div>
      <div style={{ position: 'absolute', top: 15, right: 10 }}>
        {
        loading ? (
          <Spinner style={{
            height: 30, width: 30,
          }}
          />
        ) : (
          <Button
            variant={ButtonVariants.Icon}
            onClick={onDeleteClick}
          >
            {deleteIcon}
          </Button>
        )
      }

      </div>
    </Card>
  );
};
