import { Props } from './props';
import { ErrorMessage, Title } from './styled';

export const FormControl = ({
  children, title, errorMessage, style, ...rest
}: Props) => (
  <div {...rest} style={{ textAlign: 'left', ...style }}>
    <Title>{title}</Title>
    {children}
    <ErrorMessage>
      {errorMessage}
    </ErrorMessage>
  </div>
);
