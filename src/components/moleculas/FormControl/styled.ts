import { colors } from 'shared/colors';
import styled from 'styled-components';

export const Title = styled.p`
    font-size: 14px;
    font-weight: 500;
    color: ${colors.textLight}
`;
export const ErrorMessage = styled.p`

    margin: 5px 0 0 0 ;
    font-size: 12px;
    font-weight: 400;
    color: ${colors.error};
`;
