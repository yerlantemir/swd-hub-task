import { Button } from 'components/atoms';
import { ButtonVariants } from 'components/atoms/Button/types/ButtonVariantsEnum';
import { backIcon } from 'icons';
import { useHistory } from 'react-router';
import { Props } from './props';
import { Wrapper } from './styled';

export const MainTemplate = ({
  children, noBackButton, title, ...rest
}: Props) => {
  const history = useHistory();
  const onBackClick = () => {
    history.goBack();
  };

  return (
    <Wrapper {...rest}>
      {!noBackButton
      && (
      <Button onClick={onBackClick} variant={ButtonVariants.Icon} style={{ position: 'absolute', left: 40, top: 20 }}>
        {backIcon}
      </Button>
      )}
      <h1>{title}</h1>
      {children}
    </Wrapper>
  );
};
