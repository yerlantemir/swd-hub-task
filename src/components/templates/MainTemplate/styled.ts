import { colors } from 'shared/colors';
import styled from 'styled-components';

export const Wrapper = styled.div`
    padding: 40px;
    background-color: ${colors.white};
    position: relative;
    text-align: left;
`;
