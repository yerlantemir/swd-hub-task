import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {noBackButton?: boolean, title: string}
