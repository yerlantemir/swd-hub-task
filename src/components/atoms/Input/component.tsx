import { forwardRef } from 'react';
import { Styled } from './styled';
import { Props } from './props';

export const Input = forwardRef<HTMLInputElement, Props>(
  (props: Props, ref) => <Styled {...props} ref={ref} />,
);
