import styled from 'styled-components';

export const Styled = styled.div`
    padding: 24px;
    border-radius: 16px;
    box-shadow: 0px 0px 1px rgba(12, 26, 75, 0.24), 0px 3px 8px -1px rgba(50, 50, 71, 0.05);
    background-color:  #f7fafc;
`;
