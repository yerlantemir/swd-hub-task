import { Styled } from './styled';
import { Props } from './props';

export const Card = (props: Props) => <Styled {...props} />;
