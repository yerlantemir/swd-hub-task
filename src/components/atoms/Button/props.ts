import { ButtonHTMLAttributes } from 'react';
import { ButtonVariants } from './types/ButtonVariantsEnum';

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
    variant?: ButtonVariants
}
