import styled, { css } from 'styled-components';
import { colors } from '../../../shared/colors';
import { Props } from './props';
import { ButtonVariants } from './types/ButtonVariantsEnum';

export const Styled = styled.button<Props>`
    border: none;
    ${(props) => props.variant === ButtonVariants.Primary && css`
        border-radius: 8px;
        color: ${colors.white};
        background-color: ${colors.primary};
        font-size: 14px;
        font-weight: 600;
        padding: 10px 20px;
        cursor: pointer;

        &:hover {
            background: ${colors.white};
            color: ${colors.primary};
            border: 1px solid ${colors.primary};
        }

        &:active {
            background: ${colors.primary};
            color: ${colors.white};
            opacity: 0.8;
        }
    `}
    ${(props) => props.variant === ButtonVariants.Icon && css`
        cursor: pointer;
        background-color: transparent;
        transition: all .2s ease-in-out;
        padding: 0;
        margin: 0;

        &:active {
            transform: scale(1.2);
            opacity: 0.8;
        }
    `}
`;
