import { forwardRef } from 'react';
import { Props } from './props';
import { Styled } from './styled';
import { ButtonVariants } from './types/ButtonVariantsEnum';

export const Button = forwardRef<HTMLButtonElement, Props>(
  (props: Props, ref) => <Styled ref={ref} {...props} />,
);

Button.defaultProps = {
  variant: ButtonVariants.Primary,
};
