import styled from 'styled-components';

export const StyledSpinner = styled.div`
  border: 1px solid black;
  border-radius: 50%;
  border-top-color: white;
  background-color: transparent;
  left: 25%;
  right: 25%;
  top: 25%;
  bottom: 25%;
  margin: auto;
  width: 100%;
  height: 100%;
  transition: opacity 250ms;
  animation: rotate-spinner 1s linear;
  animation-iteration-count: infinite;

  @keyframes rotate-spinner {
    100% {
      transform: rotate(360deg);
    }
  }
`;
