import { FC, HTMLAttributes } from 'react';
import { StyledSpinner } from './styled';

export const Spinner: FC<HTMLAttributes<HTMLDivElement>> = ({
  style,
  ...rest
}) => (
  <div
    {...rest}
    style={{
      ...style,
      display: 'inline-block',
    }}
  >
    <StyledSpinner />
  </div>
);
