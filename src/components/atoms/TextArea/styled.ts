import styled from 'styled-components';

export const Styled = styled.textarea`
  box-shadow: 0px 1px 2px rgba(50, 50, 71, 0.08), 0px 0px 1px rgba(50, 50, 71, 0.2);
  border-radius: 6px;
  border: 1px solid black;
  padding: 10px 5px;

  ::placeholder {
    color: #7a828a;
  }

`;
