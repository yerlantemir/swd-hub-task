import { forwardRef } from 'react';
import { Props } from './props';
import { Styled } from './styled';

export const TextArea = forwardRef<HTMLTextAreaElement, Props>(
  ({ ...props }, ref) => <Styled ref={ref} {...props} />,
);
