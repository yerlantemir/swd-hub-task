import { FC } from 'react';
import { checkedIcon, uncheckedIcon } from 'icons';
import { Props } from './props';
import { Wrapper } from './styled';

export const Checkbox: FC<Props> = ({
  invalid,
  checked,
  setChecked,
  onChecked,
  className,
  cantUncheck,
  style,
  disabled,
  ...rest
}) => (
  <Wrapper
    onClick={(e) => {
      e.preventDefault();
      if (!cantUncheck && !disabled) {
        onChecked?.(!checked);
        setChecked?.(!checked);
      }
    }}
    {...rest}
  >
    {checked ? checkedIcon : uncheckedIcon}
  </Wrapper>
);
