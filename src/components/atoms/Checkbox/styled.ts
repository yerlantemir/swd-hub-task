import styled from 'styled-components';

export const Wrapper = styled.button`
  border: none;
  cursor: pointer;
  padding: 0;
  margin: 0;
`;
