import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLButtonElement> & {
  checked: boolean;
  // eslint-disable-next-line no-unused-vars
  setChecked?: (val: boolean) => void;
  invalid?: boolean;
  // eslint-disable-next-line no-unused-vars
  onChecked?: (val: boolean) => void;
  cantUncheck?: boolean;
  disabled?: boolean;
};
