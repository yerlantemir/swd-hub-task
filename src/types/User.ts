export type User = {
    userId: number;
    firstName: string;
    lastName: string;
    birthDate: string;
    gender: 'male' | 'female';
    job: string;
    biography: string;
    isActive: boolean
}
