export const colors = {
  primary: '#4c6FFF',
  secondary: '#e4ecf7',
  dark: '#16192c',
  white: '#FFFFFF',
  error: '#F16063',
  textLight: '#425466',
};
