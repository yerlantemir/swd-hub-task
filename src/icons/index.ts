export { default as deleteIcon } from './delete';
export { default as backIcon } from './back';
export { default as editIcon } from './edit';
export { default as checkedIcon } from './checked';
export { default as uncheckedIcon } from './unchecked';
