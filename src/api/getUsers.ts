import { UserType } from 'types';
import { configuredAxios } from './shared/configuredAxios';

export const getUsers = async (): Promise<UserType[]> => {
  const resp = await configuredAxios.get('/contact');
  const result = resp.data.map((user: any) => ({
    userId: user.id,
    firstName: user.first_name,
    lastName: user.last_name,
    birthDate: user.birth_date,
    gender: user.gender,
    isActive: user.is_active,
    job: user.job,
    biography: user.biography,
  }));
  return result;
};
