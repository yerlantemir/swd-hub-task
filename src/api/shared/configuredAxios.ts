import axios from 'axios';

export const configuredAxios = axios.create({
  baseURL: 'https://frontend-candidate.dev.sdh.com.ua/v1/',
});
