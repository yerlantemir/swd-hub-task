import { configuredAxios } from './shared/configuredAxios';

export const deleteUser = async (id: number): Promise<void> => {
  await configuredAxios.delete(`/contact/${id}`);
};
