import { UserType } from 'types';
import { configuredAxios } from './shared/configuredAxios';

export const createUser = async (data: Partial<UserType>) => {
  const userData = {
    first_name: data.firstName,
    last_name: data.lastName,
    birth_date: data.birthDate,
    is_active: data.isActive,
    gender: data.gender,
    job: data.job,
    biography: data.biography,
  };
  await configuredAxios.post('/contact/', userData);
};
