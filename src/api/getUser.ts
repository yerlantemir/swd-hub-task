import { UserType } from 'types';
import { configuredAxios } from './shared/configuredAxios';

export const getUser = async (id: number): Promise<UserType> => {
  const resp = await configuredAxios.get(`/contact/${id}`);
  const user = resp.data;
  const result = {
    userId: user.id,
    firstName: user.first_name,
    lastName: user.last_name,
    birthDate: user.birth_date,
    gender: user.gender,
    isActive: user.is_active,
    job: user.job,
    biography: user.biography,
  };
  return result;
};
