import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import {
  UserDetails, Users, CreateUser, UpdateUser,
} from 'components/pages';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Users />
          </Route>
          <Route exact path="/users/create">
            <CreateUser />
          </Route>
          <Route exact path="/users/:id">
            <UserDetails />
          </Route>
          <Route path="/users/:id/edit">
            <UpdateUser />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
